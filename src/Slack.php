<?php namespace CombustibleLabs\Slack;

use Httpful\Request;

class Slack
{

    public $hook;
    public $channel;
    public $message;
    public $username;
    public $icon_url;
    public $icon_emoji;

    /**
     * Create a new Slack instance.
     *
     * @param  string  $hook
     * @param  mixed  $channel
     * @param  string  $message
     * @param  string  $username
     * @param  string  $icon_url
     * @param  string  $icon_emoji
     * @return void
     */
    public function __construct($params)
    {
        $this->hook = $params['hook'];
        $this->channel = $params['channel'];
        $this->message = $params['message'];
        $this->username = $params['username'];
        if (isset($params['icon_url'])) {
            $this->icon_url = $params['icon_url'];
        } else {
            $this->icon_url = null;
        }
        if (isset($params['icon_emoji'])) {
            $this->icon_emoji = $params['icon_emoji'];
        } else {
            $this->icon_emoji = null;
        }
    }
    /**
     * Create a new Slack message instance.
     *
     * @param  string  $hook
     * @param  mixed   $channel
     * @param  string  $message
     * @return \CombustibleLabs\Slack
     */
    public static function make($params=null)
    {
        return new static($params);
    }

    /**
     * Send the Slack message.
     *
     * @return void
     */
    public function send()
    {
        $message = $this->message ?: '';

        if ($this->icon_url) {
            $payload = ['text' => $message, 'channel' => $this->channel, 'username' => $this->username, 'icon_url' => $this->icon_url];
        } elseif ($this->icon_emoji) {
            $payload = ['text' => $message, 'channel' => $this->channel, 'username' => $this->username, 'icon_emoji' => $this->icon_emoji];
        } else {
            $payload = ['text' => $message, 'channel' => $this->channel, 'username' => $this->username];
        }
        Request::post("{$this->hook}")->sendsJson()->body($payload)->send();
    }

}
