# Combustible Labs - Slack Class

Based off of the Slack Class from the laravel package: [Envoy](https://github.com/laravel/envoy).

I've made small modifications to the Slack class to make it easier to change some of the Slack message settings.

please note: This is a very simple implementation which works, however for a more robust and better documented Slack integration I would recommend [cleentfaar/slack](https://github.com/cleentfaar/slack)

## Installation

to composer.json:

require: 
```
#!code

                "combustiblelabs/slack": "0.1.*"
```


repositories (though this has been placed on packagist.org so you should no longer need to add the repository): 

```
#!code

                {"type": "git",
                "url": "https://bitbucket.org/combustiblelabs/slack"}
```

Then run...

                   composer update

### Usage

$channel can either be a slack channel name ( #channel-name ) or a user ( '@'username ).

$username is the name for the Slack Bot that will show up in your slack messages window.
                


```
#!code

                use Slack;

                $sm = Slack::make(
                    ['hook' => env('SLACK_INCOMING_WEBHOOK_URL'),
                        'message' => $message,
                        'channel' => $channel,
                        'username' => $username,
                        'icon_emoji' => ":bowtie:"]);
                $sm->send();
```


If you'd like you may alias the class by placing this in app.php...

                'Slack'     => CombustibleLabs\Slack\Slack::class,